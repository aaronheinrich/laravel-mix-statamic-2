# Laravel Mix for Statamic 2

A modification of https://github.com/edcs/laravel-mix-statamic to allow for mix-manifest.json to be exported in the public assets folder.

The original will only look for mix-manifest.json in the project root directory. This version is modified to expect the file at the path defined in the `path` tag parameter value. If no such parameter value is defined the file is expected in the project root.

# Tag Example

{{ laravel_mix:css path="site/themes/theme/assets/dist/" src="app" }}

This will expect the mix-manifest.json in "site/themes/theme/assets/dist/" and expect the CSS file at "site/themes/theme/assets/dist/css/app.css".

Note: the `src` parameter is not documented in the original's README, but allows you to specify the name of the asset file. By default the addon expects the name of the file to be the same as the name of the theme. 
